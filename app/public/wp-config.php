<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Iq1CvYh4VWVbLJpyp9IkbeF3Hxss18jxceuqccfdKDzp+njMlYb5l8unqLiXhegr2OmOglzwJLb9WsEkIKnUJg==');
define('SECURE_AUTH_KEY',  '+TR/h0P7PWjphRcO5M5QbhbIHUAc5j6Y01ZSXyFK1mtV5t6jZ5S3qP5Qpj9a58EmhbJgOHlGbsNV1VP2iLKXkQ==');
define('LOGGED_IN_KEY',    'OsShSe37PaWLZoh0osLBbKap9FV5tfJEvUePIFDdgPGg1cJsJCQrR3qGBJp0xID3c9ILwtlQ0ZvDG+Y3dZ8jpQ==');
define('NONCE_KEY',        'Y32I9CgGzw3E2Z6rGqVxMAOKlOUBv0SU/ULYm94dchHxi5zNdlRj5O+/njlsKu4LgHtMyCak6rqF18BcI8huRg==');
define('AUTH_SALT',        '+YLKM1HFond0jyMsBDa0kyHnCl6Z9zIPGtBulkMV6wWKB4OvMz+IIjPMze2BpgDoW+XvZuF0ENWtm2QIQloAlA==');
define('SECURE_AUTH_SALT', 'NCkwqH+xmyuzyKTk1rnBTUfjhS40f1BKZs2Je+XMgb2NbGvQOog/a9+9yyPB3AcYbAusJRW5wd5OJzKVQWbuLA==');
define('LOGGED_IN_SALT',   'rJSzwUOh3xEpCwCYG//s5mHEIv6/kOYOweZO9EpD01nG4z0myVlLFpKFOhSOeZtSA90zs4qM20lRq1gueAbvtQ==');
define('NONCE_SALT',       'gUlBMUU7re5KLz9fTCUMZZdT2HVU6Hf0TF68nKeQZNZHqc6ltsT0hMKxBZw4aXHfoQMb6jsYEFNPELcoCNm/ZA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
